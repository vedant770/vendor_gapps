# Copyright (C) 2020 shagbag913
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# GApps usecase-specific Makefile fixups

MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

function override_module() {
    local moduleName="$1"

    shift
    for module in $@; do
        local overrideList="$overrideList\"$module\""
    done
    overrideList=$(echo "$overrideList" | sed 's/""/", "/g')
    overrideList="[$overrideList]"

    sed -i '/name: "'"$moduleName"'",/a \	overrides: '"$overrideList"',' common/Android.bp
}

function enable_dexpreopt() {
    local moduleName="$1"
    local shouldDexpreopt="$2"
    local moduleFound="false"
    local dexPreoptFound="false"

    while IFS= read -r line; do
        if ! echo "$line" | grep -q "name: \"$moduleName\""; then
            if [ "$moduleFound" != "true" ]; then
                echo "$line" >> "$MY_DIR"/common/Android.bp.tmp
                continue
            fi
        else
            moduleFound="true"
        fi

        if [ "$dexPreoptFound" = "true" ]; then
            line=$(echo "$line" | sed 's/false/true/')
            dexPreoptFound="false"
        fi

        if [ "$moduleFound" = "true" ] && echo "$line" | grep -q "dex_preopt"; then
            dexPreoptFound="true"
        fi

        if echo "$line" | grep -q "}"; then
            moduleFound="false"
        fi

        echo "$line" >> "$MY_DIR"/common/Android.bp.tmp
    done < "$MY_DIR"/common/Android.bp
    mv "$MY_DIR"/common/Android.bp.tmp "$MY_DIR"/common/Android.bp
}

# $1: Module/file names
# $2: Conditionalize flag
function conditonalize() {
    echo "" >> "$MY_DIR"/common/common-vendor.mk
    echo "$2" >> "$MY_DIR"/common/common-vendor.mk

    for i in $(echo "$1"); do
        local basename=$(echo "$i" | cut -d ';' -f 2)
        # basename to remove slashes in COPY variables
        sed -i "/$(basename $basename)/d" "$MY_DIR"/common/common-vendor.mk
        if echo "$i" | grep -q "MODULE;"; then
            echo 'PRODUCT_PACKAGES += \' >> "$MY_DIR"/common/common-vendor.mk
            echo "    $basename" >> "$MY_DIR"/common/common-vendor.mk
        else
            echo 'PRODUCT_COPY_FILES += \' >> "$MY_DIR"/common/common-vendor.mk
            echo "    $basename" >> "$MY_DIR"/common/common-vendor.mk
        fi
    done

    echo 'endif' >> "$MY_DIR"/common/common-vendor.mk
}

# Conditionalize NGA
#conditonalize 'MODULE;NgaResources COPY;vendor/gapps/common/proprietary/product/etc/sysconfig/nga.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nga.xml' 'ifeq ($(TARGET_SUPPORTS_NGA),true)'
conditonalize 'MODULE;GoogleCamera' 'ifneq ($(filter blueline bonito coral crosshatch sunfish taimen,$(BLISS_BUILD)),)'

override_module "Chrome" "HTMLViewer" "Browser2" "Jelly"
override_module "SetupWizardPrebuilt" "Provision"
override_module "Velvet" "QuickSearchBox"
override_module "GoogleDialer" "Dialer"
override_module "GoogleOneTimeInitializer" "OneTimeInitializer"
override_module "WebViewGoogle" "webview"
override_module "CalendarGooglePrebuilt" "Calendar"
override_module "Photos" "Gallery2"
override_module "PrebuiltGmail" "Email"
override_module "PrebuiltBugle" "messaging"
override_module "GoogleContacts" "Contacts"
override_module "LatinIMEGooglePrebuilt" "LatinIME"
override_module "GoogleCamera" "Camera2"

# Add `REMOVE_GAPPS_PACKAGES` logic
sed -i '0,/PRODUCT_PACKAGES/ s/PRODUCT_PACKAGES/ALL_PACKAGES/' "$MY_DIR"/common/common-vendor.mk
echo '' >> "$MY_DIR"/common/common-vendor.mk
echo 'PRODUCT_PACKAGES += $(filter-out $(REMOVE_GAPPS_PACKAGES),$(ALL_PACKAGES))' >> "$MY_DIR"/common/common-vendor.mk
rm -rf "$MY_DIR"/common/Android.mk

echo 'PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \' >> "$MY_DIR"/common/common-vendor.mk
echo '  system/app/GoogleExtShared/GoogleExtShared.apk \' >> "$MY_DIR"/common/common-vendor.mk
echo '  system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \' >> "$MY_DIR"/common/common-vendor.mk
echo '  system/etc/permissions/privapp-permissions-google.xml' >> "$MY_DIR"/common/common-vendor.mk
